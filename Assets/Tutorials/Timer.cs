using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

[EventHandlerInfo("Timers", "Simple Timer", "Executes the block after an amount of time has elapsed")]
public class Timer : EventHandler
{
    public float duration;

    private void Start()
    {
        Invoke("TimerExpired", duration);
    }

    void TimerExpired()
    {
        ExecuteBlock();
    }
}
