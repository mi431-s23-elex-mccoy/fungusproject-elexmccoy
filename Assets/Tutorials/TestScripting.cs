using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class TestScripting : MonoBehaviour
{
    public Flowchart flowchart;

    // Start is called before the first frame update
    void Start()
    {
        flowchart.SetStringVariable("MyName", "Generic");

        flowchart.ExecuteBlock("Extension");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void CallMe()
    {
        Debug.Log("Hi there");
    }
}
