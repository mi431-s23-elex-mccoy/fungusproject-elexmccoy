using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;


public class TestVariables : MonoBehaviour
{
    public Flowchart flowchart;

    // Start is called before the first frame update
    void Start()
    {
        flowchart.SetIntegerVariable("NumRocks", 5);
    }

    // Update is called once per frame
    void Update()
    {
        int numRocks = flowchart.GetIntegerVariable("NumRocks");
        Debug.Log(numRocks);
    }
}
